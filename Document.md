微型PHP框架 SixFramework 使用手册
============

### 1. 概述

+ 一个微型的超轻量级的PHP框架，实现了基本的 MVC 和路由。
+ 项目主页： [SixFramework](http://sf.mousika.cn/)

### 2. 框架内容

+ **SixFramework.php** 是整个框架的核心文件，也可以说整个框架就只有这一个文件，这个文件包含了框架的配置以及所有的类。

+ **index.php** 是入口文件，当然，你可以自定义。只要在使用框架之前 **require** 一下 **SixFramework.php** 就可以了。
一般情况下，index.php 的内容是这样子的：

```php
<?php
    // 引入框架的文件
    require 'SixFramework.php';
    
    // 启动 SfWork
    Sf::load();
    // 不加参数的效果相当于 Sf::load('main', 'index');
```

+ 配置在框架文件 **SixFramework.php** 的开头部分。建议使用框架前先浏览一下 SixFramework 的源码。

### 3. 新建一个页面

##### 3.1 创建一个控制器动作
在 **controller/MainController.php** 文件中新建一个方法：

```php
<?php
public function newPageAction(){
  echo 'This is a new page.';
}
```

打开浏览器，打开如下页面

```
http://localhost/SixFramework/index.php?c=main&a=new_page
```

如果要在其他页面创建超链接，可以用框架的路由构建函数：

```php
<a href="<?php echo Sf::router('main', 'new_page'); ?>">Go to newPage.</a>
```

当然，这是最简单的例子，下面来添加视图。

##### 3.2 创建视图
在 **view/Main/** 目录下创建一个名为 **newPage.php** 的文件，在里面输入：

```html
<html>
<head>
    <title>SixFramework Demo</title>
</head>
<body>
    <h1>Hello, Tony!</h1>
    <hr />
    <p>SixFramework</p>
</body>
</html>
```

回到控制器 **MainController.php** 文件，修改 newPageAction 方法：

```php
<?php
public function newPageAction() {
    //echo 'This is a new page.';
    $this->getView(__FUNCTION__);
}
```

保存，刷新第一步打开的页面，就可以看到视图文件加载后的效果。

##### 3.3 视图内的变量使用
现在来演示如何往视图传入变量。假设你有一个数组和一个字符串变量需要在视图中显示，我们可以在控制器中这么做：

```php
<?php
public function newPageAction() {
    //echo 'This is a new page.';
    $me = 'Tony';
    $girls = array(
            'Nido', 'Cindy', 'Kate', 'Lily', 'Lucy', 'Hebe'
        );
    $this->getView(__FUNCTION__, array(
            'me' => $me,
            'girls' => $girls,
        ));
}
```

在视图文件 **view/Main/newPage.php** 里，直接这样用就可以了：

```html
<p>My name is <?php echo $me; ?>.</p>
<ul>
<?php foreach($girls as $g){ ?>
    <li><?php echo $g; ?></li>
<?php } ?>
</ul>
```

### 4. 数据库操作

##### 4.1 创建表
这一节开始演示数据库操作。首先，我们得创建一个表，例如著名的 student 表，建表语句如下：

```sql
CREATE TABLE `student` (
  `s_id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NULL,
  `password` VARCHAR(50) NULL,
  `age` INT NULL,
  `intro` TEXT NULL,
  PRIMARY KEY (`s_id`));
```

表的字段有学生id，用户名，密码，年龄以及个人简介。

##### 4.2 创建 Model
根据 student 表创建 Model，一个 Model 的样板如下：

```php    
<?php
class model_name extends Db {
    public function __construct() {
        parent::__construct();
        $this->table = __CLASS__;
        $this->pk = 'primary_key';
    }
    public $properties = array(
        'properties_name_1',
        'properties_name_2',
        'properties_name_3',
    );
}
```

Copy一下模板，我们很快写出 student 的 Model：

```php
<?php
class student extends Db {
    public function __construct() {
        parent::__construct();
        $this->table = __CLASS__;
        $this->pk = 's_id';
    }
    public $properties = array(
        's_id',
        'username',
        'password',
        'age',
        'intro',
    );
}
```

需要注意的是类名必须与表名相同。

##### 4.3 添加一条数据

+ 注意：在操作数据库之前，请先把数据库配置信息修改好，确保能连接到数据库。

下面我们在 controller 目录下新建一个控制器 **StudentController.php**，代码如下：

```php
<?php
class MainController extends Controller {
?>
```

添加一个学生管理的视图，html 代码看起来是这样子的：

```html
<h2>添加学生</h2>
<form method="POST" action="<?php echo Sf::router('student', 'add'); ?>">
<p>Username: <input type="text" name="username"></p>
<p>Password: <input type="password" name="password"></p>
<p>Age: <input type="text" name="age"></p>
<p>Introduction: <input type="text" name="intro"></p>
<p><input type="submit" value="添加"></p>
</form>
```

这里需要注意的是表单提交的地址 **<?php echo Sf::router('student', 'add'); ?>** ，地址是由 Sf 类的 **router** 方法生成的，生成的效果如下：

```
index.php?c=student&a=add
```

+ 暂时还不支持添加参数，只能自己手动添加了。

负责添加的动作的代码如下：

```php
<?php
public function addAction() {
    // 实例化一个学生表的 model
    $m = $this->getModel('student');
    // 设置属性
    $m->setUsername($_POST['username']);
    $m->setPassword($_POST['password']);    
    $m->setAge($_POST['age']);
    $m->setIntro($_POST['intro']);
    // 保存
    $m->save();
}
```

+ 如果需要给密码加密，可以使用 **Utils::encrypt($pwd[,$salt])** 函数。另外就是数据验证需要自己动手。

##### 4.4 查询
对于查询，**SixFramework** 提供了 **find($condition)**，**findAll()**，**findOneByPk($id)** 等方法，下面分别举例。

+ **findOneByPk($id)** 根据主键查询单条记录；
+ **findAll()** 查询出所有的记录；
+ **find($condition)** 根据条件语句查询部分记录；

```php
<?php
public function listAction() {
        // 实例化 Model
        $m = $this->getModel('student');
        // 主键单条记录查询
        $s_id = 1;
        $student = $m->findOneBySId($s_id);
        // 查询所有
        $students = $m->findAll();
        // 条件查询
        $condition = 'where age = 22';
        $students_age_22 = $this->find($condition);
        // 传入数据，加载视图
        $this->getView(__FUNCTION__, array(
                'student' => $student,
                'students' => $students,
                'students_age_22' => $students_age_22,
            ));
    }
```

##### 4.5 删除
删除也很简单，实例化模型后，直接调用 **delete($id)** 方法即可，代码如下：

```php
<?php
public function deleteAction() {
    $s_id = $_GET['s_id'];
    $m = $this->getModel('student');
    $m->delete($s_id);
}
```

##### 4.6 修改
修改需要注意的是，跟添加一条记录不同，添加的时候直接设置 $model 的属性就可以了，
但修改需要先查询出需要修改的记录（返回的结果是数组形式），然后修改这个数组，最后
用 update($arr) 方法保存修改即可：

```php
<?php
public function updateAction() {
    $m = $this->getModel('student');
    $student = $m->findOneBySId($_POST['s_id']);
    $student['username'] = $_POST['username'];
    $student['age'] = $_POST['age'];
    $m->update($student);
}
```

### 5. 其他

##### 5.1 命名规范
+ 控制器命名采用首字母大写的驼峰命名法，并且以 **Controller** 结尾，比如 MainController，UserController 等；
+ 动作命名采用首字母小写的驼峰命名法，并且必须以 **Action** 结尾，比如 addAction，deleteAction 等；
+ Model命名必须与表名一致，比如有带前缀的 **tb_user** 表，则 Model 类名也为 **tb_user**；
+ 视图命名必须与动作（Action）一致，路径与控制器名称一致，具体参照 demo，这里不再赘述；
+ 类名与文件命名：类名必须跟文件名保持一致，比如文件名为 **MainController.php**，则类名为 **MainController**；
+ PS: 强烈建议所有文件采用去BOM的utf-8编码，数据库亦使用utf8，否则出现乱码不关我事~ :)
+ PS2:限于开发者的能力，本框架尚不完善，故不适合作为正式项目采用，建议做课程设计毕业设计的以及自己玩一玩的采用。

##### 5.2 手册信息

```php
<?php
/**
 * 微型PHP框架 SixFramework 使用手册
 *
 * @Author: Tony Lu <lucups@live.com>
 * @Version: 0.0.1
 * @Update: 2014-04-05
 */
```