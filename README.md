微型PHP框架 SixFramework
============

### 概述

+ **SixFramework** 一个微型的超轻量级的PHP框架，实现了基本的 MVC 和路由。
+ 项目主页： [SixFramework](http://sf.mousika.cn/)
+ 使用手册： [SixFramework Document](Document.md)