 <?php
    class student extends Db {

        public function __construct() {
            parent::__construct();
            $this->table = __CLASS__;
            $this->pk = 's_id';
        }
        
        public $properties = array(
            's_id',
            'username',
            'password',
            'age',
            'intro',
        );
    }
