# SixFramework 更新计划

1. 给 SixFramework 一个清晰的定位；
2. 更改数据库连接方式，采用 PDO 或 MySQLi；
3. 增强代码安全性（主要是防 SQL 注入）；
4. 增加新特性：模板引擎；
5. 增强路由；
6. 函数优化；