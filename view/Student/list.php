<html>
<head>
    <title>SixFramework Demo</title>
</head>
<body>
    <h1>学生管理</h1>
    <hr />

    <h2>添加学生</h2>
    <form method="POST" action="<?php echo Sf::router('student', 'add'); ?>">
    <p>Username: <input type="text" name="username"></p>
    <p>Password: <input type="password" name="password"></p>
    <p>Age: <input type="text" name="age"></p>
    <p>Introduction: <input type="text" name="intro"></p>
    <p><input type="submit" value="添加"></p>
    </form> 

    <hr />
    <h2>查询</h2> 

    <p>一个学生：<?php if($student) echo $student['username']; ?></p>
    <p>所有学生：</p>
    <ul>
        <?php foreach ($students as $s) { ?>
        <li><?php echo $s['username']; ?></li>
        <?php } ?>
    </ul>
    <p>所有年龄为22的学生：</p>
    <ul>
        <?php foreach ($students_age_22 as $sa22) { ?>
        <li><?php echo $sa22['username']; ?></li>
        <?php } ?>
    </ul>

</body>
</html>