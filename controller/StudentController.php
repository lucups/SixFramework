<?php

class StudentController extends Controller {

    public function listAction() {
        // 实例化 Model
        $m = $this->getModel('student');
        // 主键单条记录查询
        $s_id = 1;
        $student = $m->findOneBySId($s_id);
        // 查询所有
        $students = $m->findAll();
        // 条件查询
        $condition = 'where age = 22';
        $students_age_22 = $m->find($condition);
        // 传入数据，加载视图
        $this->getView(__FUNCTION__, array(
                'student' => $student,
                'students' => $students,
                'students_age_22' => $students_age_22,
            ));
    }

    public function viewAction() {
        // 实例化 Model
        $m = $this->getModel('student');
        // 主键单条记录查询
        $s_id = 1;
        $student = $m->findOneBySId($s_id);
        $this->getView(__FUNCTION__, array(
                'student' => $student,
            ));
    }

    public function addAction() {
        // 实例化一个学生表的 model
        $m = $this->getModel('student');
        // 设置属性
        $m->setUsername($_POST['username']);
        $m->setPassword($_POST['password']);    
        $m->setAge($_POST['age']);
        $m->setIntro($_POST['intro']);
        // 保存
        $m->save();
    }

    public function deleteAction() {
        $s_id = $_GET['s_id'];
        $m = $this->getModel('student');
        $m->delete($s_id);
    }

    public function updateAction() {
        $m = $this->getModel('student');
        $student = $m->findOneBySId($_POST['s_id']);
        $student['username'] = $_POST['username'];
        $student['age'] = $_POST['age'];
        $m->update($student);
    }
}