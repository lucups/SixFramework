<?php

class MainController extends Controller {

    public function indexAction() {
        $this->getView(__FUNCTION__);
    }

    public function newPageAction() {
        //echo 'This is a new page.';
        
        $me = 'Tony';
        $girls = array(
                'Nido', 'Cindy', 'Kate', 'Lily', 'Lucy', 'Hebe'
            );

        $this->getView(__FUNCTION__, array(
                'me' => $me,
                'girls' => $girls,
            ));
    }
    
}